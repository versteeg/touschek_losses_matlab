
function [newlosscoor, histlossel] = traj(opticswithap, lossesperel, plane)
%% compute the lost particles coordinate in plane (1 to 6) as a structure of length the number of loss locations,
% each structure is a table of size the number of lost particles x the
% coordinate history in lossesperel.
% histlossel is a struct array containing the indexes of the last 10
% elements for each loss location.

    % extract coordinate
    losscoor = arrayfun( @(el) arrayfun(@(part) el.coordinates(plane,part,:), 1:size(el.coordinates,2), 'Un', 0), lossesperel, 'UniformOutput', false );
    lossel   = arrayfun( @(el) el.element, lossesperel, 'UniformOutput', false );

    % reshape to get table of size npart*nhist at each element
    losscoor = cellfun( @(el) cell2mat( cellfun(@(part) reshape(part,1,size(part,3),[]), el , 'Un', 0)' ), losscoor, 'UniformOutput', false );
   
    % erase not lost particles
    todrop = cellfun(@(el2) ismember(0,el2), cellfun(@(el) size(el), lossel, 'UniformOutput', false ) ) | ...
        (cell2mat(cellfun(@(el)any(isnan(el)),lossel, 'UniformOutput', false )) .* ...
         cell2mat(cellfun(@(el) length(el), lossel, 'UniformOutput', false ))==1 );
    lossel(todrop) = [];
    losscoor(todrop) = [];
    lossel = cell2mat(lossel)';
    losscoor = cell2mat(losscoor');

    
    losscoor( isnan(lossel), : ) = [];
    lossel( isnan(lossel) ) = [];
    
    % attribute the losses on aperture to the related thick element
    aplocmask = ismember( lossel, findcells(opticswithap, 'RelatedElement') );
    corrthickel = arrayfun( @(idxap) opticswithap{idxap}.RelatedElement, lossel(aplocmask) );
    lossel(aplocmask) = corrthickel;

    % reorganize with respect with loss location
    [sortedlossel, idx] = sort(lossel);
    sizecells = cell2mat(arrayfun(@(el)length(find(sortedlossel==el)), unique(sortedlossel), 'un', 0));
    newlosscoor = mat2cell(losscoor(idx,:), sizecells);
    histlossel = arrayfun(@(el) mod(reverse(el-(0:9)), length(opticswithap)), unique(sortedlossel), 'un', 0);
    histlossellastel = cellfun(@(el) el==0,histlossel,'un',0);    
    histlossel = cellfun(@(el,mask) el+mask*length(opticswithap),histlossel,histlossellastel,'un',0);
end



% [newlosscoorx, histlosselx] = traj(LatticeSmallerDriftswithap, Lattice_lossinfo, 1);
% [newlosscoory, histlossely] = traj(LatticeSmallerDriftswithap, Lattice_lossinfo, 3);
% PlotTrajHist(LatticeSmallerDriftswithap, histlossel, newlosscoorx, newlosscoory)

