function [ losslocations, lossinfo, Tl ] = TouschekLossMap( opticswithap, constants,...
     first, last, Nturns, Npart, startindex, MomAcc, marginonmomacc, switchtrajy, Ibunch, varargin )
%[ losslocations, lossinfo, Tl ] = TouschekLossMap( opticswithap, constants,...
%     first, last, Nturns, Npart, startindex, MomAcc, marginonmomacc, switchtrajy, Ibunch, varargin )
%% Generates Touschek particles at elements first to last and returns the
%% resulting loss map around opticswithap
%
% varargin may contain sigmas to impose a different value than the equilibrium calculated by atx, and
% epsy. For halo calculation put varargin{3}='halo'
%
% constants.r0 = 2.81794e-15;
% constants.eulercst = 0.577215;
% constants.c = 2.99792e8;
% constants.m0 = 511.e3;
% constants.gamma = atenergy(LatticeSmallerDriftswithap) / constants.m0;
% constants.beta = sqrt(1 - 1 / constants.gamma^2);
%


lengthlist = cellfun(@(el) el.Length, opticswithap);
maskthick = lengthlist > 1e-9;
thickelem = find(maskthick);

listelem = thickelem( ismember( thickelem, (first:last) ) ); %% 

losslocations = [];
lossinfo = [];
Tl=0;
haloswitch=0;

if ~isempty(listelem) 
    
    disp('   Computing lattice parameters...')
    
    warning('off', 'all');
    if isempty(findcells(opticswithap,'PassMethod', '\w*Rad\w*'))
        [~, param] = atx(opticswithap, 0, 1:length(opticswithap));
    else
        [~, param] = atx(opticswithap, 0, 1:length(opticswithap), @noradon);        
    end
%    [beamdata, param] = atx(opticswithap);
    sigmas = param.blength; 
    if isempty(varargin), 
        emity = param.modemittance(2);
    else
        if strcmp(varargin{1}, 'auto'), sigmasratio = 1; 
        else sigmas = varargin{1}; sigmasratio = varargin{1}/param.blength;
        end
        if strcmp(varargin{2}, 'auto'), emity = param.modemittance(2); else emity = varargin{2};end
    end
    
    if nargin>13 && strcmp(varargin{3},'halo')
        haloswitch=1;
    end 
    
    warning('on', 'all');
    
    
    

    [M66, Ts, co]    = findm66(atradoff(opticswithap,''), listelem);
    a66=amat(M66);
    [G1, G2, G3] = find_inv_G_fromA(a66);
    S66 = jmat(3) ;
    beam66_s0  = -S66 * ( param.modemittance(1)*G1 + emity*G2 + sigmas*param.espread*G3 ) * S66;
%     beam66_s0(6,:) = sigmasratio * beam66_s0(6,:);   %  wrong, modified on 09/06/2015 according to SigmaMatrix6D.nb

    beam66_s0(6,6) = beam66_s0(6,6) - param.blength^2 + sigmas^2;
    beam66_s0 = triu(beam66_s0) + triu(beam66_s0,1)';


    listbeam66 = arrayfun( @(el) Ts(:,:,el) * beam66_s0 * Ts(:,:,el).', 1:length(listelem), 'Uniformoutput', false );
%    listbeam66 = arrayfun( @(el) eye(6) * beam66_s0 * eye(6).', 1:length(listelem), 'Uniformoutput', false );

    disp('   Computing average Touschek lifetime and element by element probability of interaction...')    
    
    newmomacc = interp1( findspos(opticswithap,startindex), MomAcc, findspos(opticswithap,(1:length(opticswithap))), 'linear', 'extrap' );

    [Tl, contributionsTL] = TouschekPiwinskiLifeTime_r(opticswithap, newmomacc, Ibunch,...
        listelem', param.modemittance(1), emity, 'quad', param.espread, sigmas);
    [~, contributionsTLmargin] = TouschekPiwinskiLifeTime_r(opticswithap, (1-marginonmomacc)*newmomacc, Ibunch,...
        listelem', param.modemittance(1), emity, 'quad', param.espread, sigmas);

    
    disp( '   Calculating loss map...')

    if ~haloswitch
    [losslocations, lossinfo] = arrayfun(@(counter) TouschekPerElement( opticswithap, ...
            newmomacc, listelem(counter), contributionsTL(counter),...
            Nturns, Npart, constants, switchtrajy, co(:, counter), listbeam66{counter}, ...
            marginonmomacc, contributionsTLmargin(counter) ), 1:length(listelem), 'UniformOutput',false);

    else
    [losslocations, lossinfo] = arrayfun(@(counter) TouschekPerElement( opticswithap, ...
            newmomacc, listelem(counter), contributionsTL(counter),...
            Nturns, Npart, constants, switchtrajy, co(:, counter), listbeam66{counter}, ...
            'halo' ), 1:length(listelem), 'UniformOutput',false);
    end
else
    disp('Empty element list or input data missing.')
end
end








