function [ losslocations, lossinfo ] = LossMap( opticswithap,...
    sumgasdensity, plane, anglevselement, startindex, Nturns, Npart, first, last )
%% Generates the losses from residual gas scattering around the ring.
%   opticswithap is the lattice structure, with physical aperture elements,
%   sumgasdenity is the table of double sum Sum(atoms)Sum(gas) Zj^2 alphaij
%   pi/kT at each element (ResidualGasElbyEl.m),
%   plane is 'X' or 'Y'
%   anglevselement is the angle limit from which particles are considered to
%   be lost if scattered at element #startindex (thick elements, length(anglevselement) < length(opticswithap)) (atvacuumlosses.m),
%   Nturns is the number of turns lost particles will be tracked to record
%   the loss
%   Npart is the number of particles generated at each element
%   varargin could be a file name to save losslocations and lossinfo



r0 = 2.81794e-15;
c = 2.99792e8;
gamma = atenergy(opticswithap) / 511.e3;
beta = sqrt(1 - 1 / gamma^2);
cstfactor = (2*pi*r0^2*c) / (gamma^2*beta^3);


% Losses around the ring from all elements
lengthlist = cellfun(@(el) el.Length, opticswithap);
maskthick = lengthlist > 1e-9;
thickelem = find(maskthick);

listelem = thickelem( ismember( thickelem, (first:last) ) ) %% 

losslocations = cell(length(listelem),1);
lossinfo = cell(length(listelem),1);

absangle = cellfun(@(sign)min(abs(sign)),mat2cell(anglevselement,ones(length(anglevselement),1)));
elbyeltauinv = cstfactor ./ absangle.^2 .* sumgasdensity(maskthick);

for counter = 1:length(listelem)
    disp([num2str(counter/length(listelem)*100) '% '])
    el = listelem(counter);
    gasdata = sumgasdensity(el);
    maxangle = anglevselement(startindex==el, :);
    tauinvel = elbyeltauinv(startindex==el);
    [losslocations{counter}, lossinfo{counter}] = ScatteringPerElement( opticswithap, el, gasdata, maxangle, tauinvel, Nturns, Npart, plane );
end




end

