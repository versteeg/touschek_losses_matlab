
function [ meantauinv, meantauinv1, meantauinv2 ] = atFullVacuumLifetime( optics,...
    aplimits, pressureinmbar, gascomp, plane, momacc, varargin )
% [ meantauinv,meantauinv1,meantauinv2, thpl, mpl ] = atFullVacuumLifetime( optics,...
%    aplimits, pressure, gascomp, plane, momacc ) 
%% atFullVacuumLifetime calculates the average vacuum beam lifetime from single Coulomb
%% scattering and Bremstrahlung with residual gaz particles along the lattice defined by optics, assuming T = 298 K (tauinv=1/tau in s-1).
%
%   aplimits are h- and v-physical aperture, each can be one 1x4-vector or a matrix length(optics)x4 giving aperture limits for each element of optics [apxm apxp apym apyp].
%   aplimits is the 2nd output of ataperturedescription. Can be empty in
%   case only the element by element lifetime is needed, than the angular acceptance must be given as last argument.
%
%   pressure in mbar(scalar or vector)
%   gascomp is a structure describing the gas constitution. It must be in the form 
%    { { {percentagegas1 [Z11 alpha11 ; Z12 alpha12 ; ...]} ; 
%        {percentagegas2 [Z21 alpha21 ; Z22 alpha22 ; ...]} ; ... {percentagegasn [...]} };
%      ... 
%      { gas composition at element N } }
%   where percentagegasi is the proportion of gas i in the residual
%   gas (<1), Zij are the atomic number of each element j present in gas i, alphaij the number of
%   atom j in a molecule of gas i. If the structure has one single element, the same gas
%   composition is assumed along the lattice, no matter the argument pressure, otherwise length(gascomp) must be equal to opticslength.
%
%   plane is the dominant plane for C. scattering losses, 'X' or 'Y', if
%   anything else it takes both contributions into account (still considered independant)
%   momacc is the negative momentum acceptance for bremstrahlung 
%
%  meantauinv = total average lifetime
%  meantauinv1 = average CS lifetime
%  meantauinv2 = average B lifetime

% 20181129 - modification to allow column/line vector as momentum
% acceptance input + took out the output of thpl and mpl (not always defined and used for debug)
% 20200408 - changed pressure to be in mbar, to avoid having to multiply by
% 100 the data from operation...


pressure = pressureinmbar*100; % converto to Pa(usi)

r0 = 2.81794e-15;
c = 2.99792e8;
gamma = atenergy(optics) / 511.e3;
beta = sqrt(1 - 1 / gamma^2);
cstfactorC = (2*pi*r0^2*c) / (gamma^2*beta^3);
cstfactorB = (4*r0^2*c) / (137);

meantauinv = [];
meantauinv1 = [];
meantauinv2 = [];
        
if ~( isscalar(pressure) || length(pressure) == length(optics) ) && ( length(gascomp) ==1 || length(gascomp) == length(optics) ) && ( isscalar(apx) || length(apx) == length(optics) ) && ( isscalar(apy) || length(apy) == length(optics) )
    fprintf( '\n Error in pressure input, cannot compute the vacuum lifetime \n' );
else 
  
    if cellfun( @(gascompel) sum(cellfun( @(gasi) gasi{1}, gascompel )) , gascomp) ~= ones(length(gascomp),1)
        fprintf( '\n Error in gas composition.\n ' );
        return
    else

        if ~isequal(size(aplimits), [1, 4]) && ~isequal(length(aplimits), length(optics)) && ~isempty( aplimits )
            fprintf( '\n Error in aperture input, cannot compute the average vacuum lifetime.\n ' );
        else
                                 
            lengthlist = cellfun( @(el) el.Length, optics );
            maskthick = lengthlist > 1e-9;
            [lindata, avebeta] = atavedata( optics, 0, maskthick );
            betaxy = cat( 1, lindata.beta );

            sumatomgasC = ResidualGasElbyEl( length(optics), pressure, gascomp, 'c' );
            
            
            if isempty( varargin ) && ~isempty( aplimits )
                
                sumbp = sum( avebeta .* sumatomgasC(maskthick,[1,1]) .* lengthlist(maskthick,[1,1]) ) ./ sum(lengthlist(maskthick));
                % adapting aperture data to the number of elements if needed
                if size(aplimits)==[1,4]
                    aplimits = repmat({aplimits},length(optics),1);
                end
                
                signap = cell2mat( cellfun(@(apelt) sign(apelt), aplimits(maskthick), 'UniformOutput', false) );
                if any(signap ~= repmat([-1 1 -1 1],length(signap),1))
                    disp('At least one jaw is blocking the beam or some aperture is missing.')
                return
                end

                
                
                ap = cell2mat( cellfun(@(apelt) [min(abs(apelt(1)),abs(apelt(2))) min(abs(apelt(3)),abs(apelt(4)))], aplimits, 'UniformOutput', false) );
                ap = ap(maskthick,:);

      
                % 1/maximum scattering angle to fit in the aperture
                minap = min(ap .* ap ./ betaxy);

                % averaged beam life time
                meantau = cstfactorC ./ minap .* sumbp;
                if plane == 'X', meantauinv1 = meantau(1);
                elseif plane == 'Y', meantauinv1 = meantau(2);
                else
                    meantauinv1 = 1/(1/meantau(1)+1/meantau(2));
                end
                
            elseif ~isempty( varargin ) && isempty( aplimits )
                th2min = min((varargin{1}.^2),[],2); % choice between x or y not included yet
                thpl =  cstfactorC .* 1./ th2min .* sumatomgasC(maskthick) ;
                
                meantauinv1 = sum(thpl.* lengthlist(maskthick)) ./ sum(lengthlist(maskthick));              
            else
                disp('Please give either aperture limits or the angular acceptance.')
                return
            end

            sumatomgasB = ResidualGasElbyEl( length(optics), pressure, gascomp, 'b' );
            if ~isscalar(momacc)
                momacc = momacc(:);        
            end
            momacc = abs(momacc);
            mpl = cstfactorB .* (4/3.*log(1./momacc)-5/6) .* sumatomgasB(maskthick) ;
                        
            meantauinv2 = sum(mpl.* lengthlist(maskthick)) ./ sum(lengthlist(maskthick));

            meantauinv = meantauinv1 + meantauinv2;
    
        end
    end
end

end

