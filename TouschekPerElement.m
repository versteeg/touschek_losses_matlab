function [ LossLocations, LossInfoElement ] = TouschekPerElement( opticswithap, MomAcc, indexelement, tauinvel, ...
    Nturns, Npart, constants, switchtrajy, co, beamsigma, varargin )
%opticswithap, ...
%            newmomacc, listelem(counter), contributionsTL(counter),...
%            Nturns, Npart, constants, switchtrajy, co(:, counter), listbeam66{counter}, marginonmomacc, contributionsTLmargin(counter)
%% Computes the losses from Touschek scattering generated at a given element.
%
%  opticswithap is the lattice structure (with aperture elements to compute the losses),
%  indexelement refers to the element to track from, where the scattering
%  occurs,
%  maxdelta is the momentum limit from which particles scattered at
%  the element are lost,
%  Nturns is the number of turns required to simulate the losses
%  (tracking).
%  Npart is the number of particles to be generated.


lengthel = opticswithap{indexelement}.Length;
LossLocations = NaN;
LossInfoElement = [];

if lengthel == 0
    disp('The element index corresponds to a thin element, please choose the thick element next to it.')
else  
    maxdeltael = MomAcc(indexelement,:);

    if nargin>10 && ~ischar(varargin{1}) && varargin{1}<=1 && varargin{1}>=0
        momaccmargin =  varargin{1};
        maxdeltaelmargin = (1-momaccmargin)*MomAcc(indexelement,:);
        tauinvelmargin = varargin{2};
        haloswitch = 0;
    elseif nargin>10 && ischar(varargin{1})
        maxdeltaelmargin = [];
        tauinvelmargin = [];
        haloswitch = 1;
        opticswithap = [{atmarker('Start')}; opticswithap];
    else
        maxdeltaelmargin = [];
        tauinvelmargin = [];
        haloswitch = 0;
    end

    LossProba = lengthel * tauinvel;
    npartlost = round( Npart * LossProba );
    Probamargin = lengthel * tauinvelmargin;
    nmargin = round( Npart * Probamargin ) - npartlost;
    
    if isempty(nmargin) || nmargin <= 0 , nmargin = 0; end
    
    if npartlost >= 1 || nmargin >= 1
        
        rin=zeros(6,npartlost);
        rinm=zeros(6,nmargin);
        
        fac = (8*pi*constants.r0^2) ;

        Sig11 = [beamsigma(1,1) beamsigma(1,3) beamsigma(1,6);...
            beamsigma(3,1) beamsigma(3,3) beamsigma(3,6);...
            beamsigma(6,1) beamsigma(6,3) beamsigma(6,6)];
        Sig12 = [beamsigma(1,2) beamsigma(1,4) beamsigma(1,5);...
            beamsigma(3,2) beamsigma(3,4) beamsigma(3,5);...
            beamsigma(6,2) beamsigma(6,4) beamsigma(6,5)];
        Sig21 = transpose(Sig12);
        Sig22 = [beamsigma(2,2) beamsigma(2,4) beamsigma(2,5);...
            beamsigma(4,2) beamsigma(4,4) beamsigma(4,5);...
            beamsigma(5,2) beamsigma(5,4) beamsigma(5,5)];
        
        MU1 = [ co(1) co(3) co(6) co(2) co(4) co(5)];
        SIGMA = [Sig11 Sig12; Sig21 Sig22];
        
        n=1; nm=1; generator=0;
        while n <=npartlost
            zerof=0;
            %  random generation of pairs of particles
            generator = generator+1;
            s = RandStream('mcg16807','Seed',indexelement*length(opticswithap)+generator);
            RandStream.setGlobalStream(s);
            part1coor = mvnrnd_r(MU1, SIGMA, 1);

            %  the second particles has x, y, and z identical for the
            %  interaction to occur
            MUx2 = (MU1(4:6)' + Sig21*inv(Sig11)*(part1coor(1:3) - MU1(1:3))')';
            SIGMAx2e = Sig22 - Sig21*inv(Sig11)*Sig12;
            part2xpcoor = mvnrnd_r(MUx2, SIGMAx2e, 1);
            part2coor = [part1coor(:,1:3) part2xpcoor(:,:)];

            relvCM = abs(part1coor(:,4) - part2coor(:,4));

            %  minimum scattering angle to lose the particles
            cosksilimit = min(arrayfun(@(side) abs(side),maxdeltaelmargin))/(constants.gamma*relvCM/2);
            
            if (0<=cosksilimit)&&(cosksilimit<=1)
                %   analtytic cross section
                f = @(x) fac/(relvCM^4)*(2-x^2)/abs(x)^3;
                %   random loss probablity
                pksi = f(cosksilimit)*rand(1, 1);
                %   cross section function interpolation to get the
                %   scattering angle
                x=linspace(0.001,1,1000); fx=arrayfun(@(cosksi) pksi-f(cosksi),x);
                zerof = find(fx>0, 1);
                %   deduce the energy loss from the scattering angle and
                %   the relative velocity
                if ~isempty(zerof) && zerof~=1
                    cosksi=x(zerof)+(x(zerof)-x(zerof-1))/2;
                    Ddeltap = constants.gamma*relvCM/2*cosksi;
                    
                    delta1 = part1coor(6) + Ddeltap;
                    delta2 = part2coor(6) - Ddeltap;
                    
                    if delta1 > maxdeltael(2) || delta1 < maxdeltael(1)
                        rin(1,n) = part1coor(1);             rin(2,n) = part1coor(4);
                        rin(3,n) = part1coor(2)*switchtrajy; rin(4,n) = part1coor(5)*switchtrajy;
                        rin(5,n) = delta1;                   rin(6,n) = part1coor(3);
                        n=n+1;
                    elseif ~isempty(maxdeltaelmargin) && (delta1 > maxdeltaelmargin(2) || delta1 < maxdeltaelmargin(1)) && nm < nmargin
                        rinm(1,nm) = part1coor(1);             rinm(2,nm) = part1coor(4);
                        rinm(3,nm) = part1coor(2)*switchtrajy; rinm(4,nm) = part1coor(5)*switchtrajy;
                        rinm(5,nm) = delta1;                   rinm(6,nm) = part1coor(3);
                        nm=nm+1;
                    end
                    
                    if delta2 > maxdeltael(2) || delta2 < maxdeltael(1)
                        rin(1,n) = part2coor(1);             rin(2,n) = part2coor(4);
                        rin(3,n) = part2coor(2)*switchtrajy; rin(4,n) = part2coor(5)*switchtrajy;
                        rin(5,n) = delta2;                   rin(6,n) = part2coor(3);
                        n=n+1;
                    elseif ~isempty(maxdeltaelmargin) && (delta2 > maxdeltaelmargin(2) || delta2 < maxdeltaelmargin(1)) && nm < nmargin
                        rinm(1,nm) = part2coor(1);             rinm(2,nm) = part2coor(4);
                        rinm(3,nm) = part2coor(2)*switchtrajy; rinm(4,nm) = part2coor(5)*switchtrajy;
                        rinm(5,nm) = delta2;                   rinm(6,nm) = part2coor(3);
                        nm=nm+1;
                    end
                else
                    % return
                end
            end
        end
                    
        opticstotrack = atrotatelattice( opticswithap, indexelement );

        %  tracking of scattered particles
        if ~haloswitch
            [~, ~, ~, LossInfoElement] = ringpass(opticstotrack, rin, Nturns, 'nhist', 10);
            LossInfoElement.ddelta = rin( 5, : );
        else

            LossInfoElement.coordinates = linepass(opticstotrack, rin, findcells(opticstotrack,'FamName','Start'));
            LossInfoElement.ddelta = rin( 5, : );            
        end
        
        if isempty(LossInfoElement)
            LossInfoElement=struct('lost', zeros(1,npartlost+nmargin), 'turn', inf(1,npartlost+nmargin),...
             'element', nan(1,npartlost+nmargin), 'coordinates', nan(6,npartlost+nmargin), 'ddelta', nan(1,npartlost+nmargin));
        end

        % get the index: indexelement-1 to have the right element index
        % after rotating, + lostpart.element => indexelement -1 + lostpart.element
        LossLocations = mod( indexelement-1 + LossInfoElement.element , length(opticswithap) );
        LossInfoElement.element = LossLocations;
        
    elseif npartlost ==0
        disp(['Zero particle scattered at element #' num2str(indexelement) ' were lost.'])
        LossInfoElement=struct('lost', zeros(1,1), 'turn', inf(1,1),...
             'element', nan(1,1), 'coordinates', nan(6,1), 'ddelta', nan(1,1));
        LossLocations = NaN;
    else
        disp(['Touschek loss rate is NaN - no particle was generated.'])
        LossInfoElement=struct('lost', NaN, 'turn', NaN,...
             'element', nan(1,1), 'coordinates', nan(6,1), 'ddelta', nan(1,1));
        LossLocations = NaN;

    end
    
end


end










